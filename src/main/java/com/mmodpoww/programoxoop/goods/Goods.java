/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmodpoww.programoxoop.goods;

/**
 *
 * @author Acer
 */
public class Goods {
    private String id;
    private String name;
    private String band;
    private double price;
    private int amount;
    
    Goods(String id,String name,String band,double price,int amount){
        this.id = id;
        this.name = name;
        this.band = band;
        this.price = price;
        this.amount = amount;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBand() {
        return band;
    }

    public void setBand(String band) {
        this.band = band;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return id+"                                          "+name                +"                                                  "+     band       +         "                                            "+  price  +"                                              "+ amount;
    }
    
}
